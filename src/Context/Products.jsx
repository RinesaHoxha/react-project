import React, { createContext, useReducer } from 'react';
import Reducer from './Reducer';

const initialState = {
    token: localStorage.getItem("token") || '',
    userInfo: localStorage.getItem("userInfo") ? JSON.parse(localStorage.getItem("userInfo")) : null
}
const Products = ({ children }) => {
  console.log("localStorage.getItem(userInfo)", localStorage.getItem("userInfo"));
    const [state, dispatch] = useReducer(Reducer, initialState);
  return (
    <Context.Provider value={[state, dispatch]}>
        {children}
    </Context.Provider>
  )
};
export const Context = createContext(initialState);
export default Products;




















