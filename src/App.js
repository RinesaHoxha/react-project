import logo from './logo.svg';
import './App.css';
import PrivateRoute from './PrivateRoute';
import Dashboard from './components/Dashboard';

import { BrowserRouter as Router, Routes, Route } from "react-router-dom"
import HomePage from "./Pages/HomePage"
import AboutPage from "./Pages/AboutPage"
import Products from './Context/Products';

const App = () => {
  return (
    <Products>

    <Router>
      <Routes>
        <Route path="/" element={<HomePage />} />
        <Route path="/about" element={<AboutPage />} />
        <Route path="/" element={<HomePage />} />
        <Route path="/dashboard" element={
          <PrivateRoute>
              <Dashboard />
            </PrivateRoute>
          } />
      </Routes>
    </Router>
    </Products>
  );
}

export default App