import React, { useContext } from 'react';
import { Navigate } from 'react-router-dom';
import { Context } from './Context/Products';

const PrivateRoute = ({ children }) => {
  const [state] = useContext(Context);
  if (state?.token) {
    return children;
  }
  return <Navigate to="/" />
}
export default PrivateRoute