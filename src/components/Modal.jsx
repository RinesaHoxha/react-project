import React from 'react';
import "./style.scss";

const Modal = (props) => {
  if (!props.isOpen) {
    return;
  }
  console.log(props);
  return (
    <div className="modal" onClick={() => props.close()}>
        {/* <span className="hidden-btn" onClick={() => props.close()}></span> */}
        <div className="modal-content" onClick={(e) => e.stopPropagation()}>
            <button className="btn-close"
              onClick={() => props.close()}
            >
              X
            </button>
            {props.blog &&
              <div>
                <h3>{props.blog.title}</h3>
                <img src={props.blog.thumbnail_url} alt='' />
                <h3>{props.blog.category}</h3>
              </div>
            }
            {props.children}
        </div>
    </div>
  )
}
Modal.defaultProps = {
  close: () => {}
}
export default Modal