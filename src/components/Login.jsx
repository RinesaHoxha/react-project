import React, { useContext, useState } from 'react';
import Input from './ContactForm/Input';
import axios from 'axios';
import { jwtDecode } from 'jwt-decode';
import { Context } from '../Context/Products';
const Login = ({ closeModal }) => {
  const [state, dispatch] = useContext(Context);
  const [formData, setFormData] = useState({
    email: "",
    password: "",
  });
  const [error, setError] = useState();
  const changes = (event) => {
    setFormData({ ...formData, [event.target.name]: event.target.value });
  };
  const send = (e) => {
    e.preventDefault();
    console.log("formData", formData);
    const url = "https://datacenter.isocium.com/authentication/login";
    axios.post(url, formData).then((res) => {
      console.log("res", res);
      if (res.data.token) {
        localStorage.setItem("token", res.data.token);
        const userInfo = jwtDecode(res.data.token);
        const objToString = JSON.stringify(userInfo)
        localStorage.setItem("userInfo", objToString);
        dispatch({
          type: "LOGIN",
          payland: {
            token: res.data.token,
            userInfo: userInfo
          }
        });
        closeModal();
      } else {
        setError(res.data.error);
        setTimeout(() => {
          setError();
        }, 5000);
      }
    }).catch((err) => console.log(err))
  }
  return (
    <div>
      <form onSubmit={send}>
        <Input
          label="Email"
          name="email"
          handleChanges={changes}
        />
        <br />
        <Input
          label="Password"
          name="password"
          handleChanges={changes}
        />
        <br />
        {error && <p style={{ color: "red" }}>{error}</p>}
        <button type="submit">Login</button>
        <br />
        <br />
      </form>
    </div>
  )
}

export default Login;