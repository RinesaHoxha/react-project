import React, { useState } from 'react'
import Input from './ContactForm/Input';
import axios from 'axios';

const SignUp = ({ goToLogin }) => {
  const [done, setDone] = useState();
  const [formData, setFormData] = useState({
    name: "",
    last_name: "",
    password: "",
    email: "",
    role_id: 1,
    status: "active"
  });
  const changes = (event) => {
    setFormData({ ...formData, [event.target.name]: event.target.value });
  };
  const send = (e) => {
    e.preventDefault();
    console.log("formData", formData);
    const url = "https://datacenter.isocium.com/authentication/signup";
    axios.post(url, formData).then((res) => {
      console.log(res);
      if (res.data.msg) {
        setDone(res.data.msg);
        setTimeout(() => {
          goToLogin();
        }, 3000);
      }
    }).catch((err) => console.log(err));
  };
  return (
    <div>
      <form onSubmit={send}>
        <Input
          label="Name"
          name="name"
          handleChanges={changes}
        />
        <Input
          label="Surname"
          name="last_name"
          handleChanges={changes}
        />
        <Input
          label="Password"
          name="password"
          handleChanges={changes}
        />
        <Input
          label="E-mail"
          name="email"
          handleChanges={changes}
        />
        <br />
        {done && <p>{done}</p>}
        <button type="submit">Register</button>
      </form>
    </div>
  )
}

export default SignUp;