import React, { useState } from 'react';
import "./style.scss";
import Input from './Input';

const ContactForm = () => {
  const [dataForm, setDataForm] = useState();
  const Changes = (event) => {
    console.log("name:", event.target.name)
    console.log("value:", event.target.value);
    // const name = event.target.name;
    // const value = event.target.value;
    // const obj = {
    //   ...dataForm,
    //   [name]: value
    // }
    // setDataForm(obj);
    // console.log(obj);
    setDataForm({ ...dataForm, [event.target.name]: event.target.value });
  };
  const sent = (e) => {
    e.preventDefault();
    console.log("dataForm", dataForm);
  }
  return (
    <div className="contact-form">
      <h3>Contact us!</h3>
      {dataForm?.firstname && <p>Firstname: {dataForm?.firstname}</p>}
      {dataForm?.lastname && <p>Lastname: {dataForm?.lastname}</p>}
      <form onSubmit={sent}>
        <Input
          label="Firstname"
          name="firstname"
          handleChanges={Changes}
        />
        <Input
          label="Lastname"
          name="lastname"
          handleChanges={Changes}
        />
        <Input
          label="E-mail"
          name="email"
          handleChanges={Changes}
        />
        <Input
          label="Phone"
          name="phone"
          handleChanges={Changes}
        />
        <button type="submit">Sent</button>
        <button type="button" onClick={sent}>Sent Data</button>
      </form>
    </div>
  )
};
export default ContactForm;






