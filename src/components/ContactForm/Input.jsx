import React from 'react';

const Input = ({ label, type, name, handleChanges, disabledBtn, value }) => {
  // const handelChanges = (event) => {
  //   console.log("name:", event.target.name)
  //   console.log("value:", event.target.value)
  // }
  return (
    <div className="form-group">
      {type !== "submit" && <label>{label}</label>}
      <br />
      <input
        type={type || "text"}
        placeholder={label}
        name={name}
        onChange={handleChanges}
        disabled={disabledBtn}
        value={value}
      />
    </div>
  )
}
export default Input;




















