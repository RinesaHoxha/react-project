import React, { useContext, useEffect, useState } from 'react';
import { Context } from '../Context/Products';
import { Link } from 'react-router-dom';
import Modal from './Modal';
import Login from './Login';
import SignUp from './SignUp';

const NavBar = () => {
  const [y, sety] = useState();
 
  const [state, dispatch] = useContext(Context);
    const [showModal, setShowModal] = useState(false);
    const [loginForm, setLoginForm] = useState(true);
    const logOut = () => {
      dispatch({
        type: "LOGIN",
        payland: {
          token: null,
          userInfo: null
        }
      });
      localStorage.removeItem("token");
      localStorage.removeItem("userInfo");
    }

  const handleScroll = () => {
    if (window.scrollY > 100) {
      sety(true);
    } else {
      sety(false);
    }
  }

  useEffect(() => {
    sety(window.scrollY);
    window.addEventListener("scroll", (e) => handleScroll(e));
  }, []);
  return (
    <nav id="tm-nav" className={`fixed w-full ${y ? "scroll" : ""}`}>
        <div className="tm-container mx-auto px-2 md:py-6 text-right">
            <button className="md:hidden py-2 px-2" id="menu-toggle"><i className="fas fa-2x fa-bars tm-text-gold"></i></button>
            <ul className="mb-3 md:mb-0 text-2xl font-normal flex justify-end flex-col md:flex-row">
                <li className="inline-block mb-4 mx-4"><a href="#intro" className="tm-text-gold py-1 md:py-3 px-4">Intro</a></li>
                <li className="inline-block mb-4 mx-4"><a href="#menu" className="tm-text-gold py-1 md:py-3 px-4">Menu</a></li>
                <li className="inline-block mb-4 mx-4"><a href="#about" className="tm-text-gold py-1 md:py-3 px-4">About</a></li>
                <li className="inline-block mb-4 mx-4"><a href="#contact" className="tm-text-gold py-1 md:py-3 px-4">Contact</a></li>
                
                <li className="inline-block mb-4 mx-4">
              {state.userInfo ? (
                <div>
                  <Link to="/dashboard">Dashboard</Link>
                  {state.userInfo?.name}
                  <button onClick={() => logOut()}>Log out</button>
                </div>
              ) : (
                <button onClick={() => setShowModal(true)}>Login</button>
              )}
            </li>
            </ul>
            <Modal isOpen={showModal} close={() => setShowModal(false)}>
            <h3>{loginForm ? "Login" : "Register"}</h3>
            {loginForm && <Login closeModal={() => setShowModal(false)} />}
            {!loginForm && <SignUp goToLogin={() => setLoginForm(true)} />}
            <button onClick={() => setLoginForm(!loginForm)}>
                {loginForm ? "Register" : "Login"}
            </button>
        </Modal>
        </div>            
    </nav>
  )
}

export default NavBar;
