import React from 'react';
import Intro from '../components/Intro';
import Menu from '../components/Menu';
import About from '../components/About';
import Contact from '../components/Contact';


const HomePage = () => {
  return (
    <div>
    <Intro />
    <Menu />
    <About />
    <Contact />

    </div>
  )
}

export default HomePage;